﻿using BlogNET.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Services
{
    public interface ISubscribeService
        : IService<Subscribe>
    {

    }

    public class SubscribeService
        : BaseService<Subscribe>, ISubscribeService
    {
        public SubscribeService(ISubscribeRepository repo) 
            : base(repo)
        {

        }
    }
}
