﻿using Microsoft.Extensions.Logging;
using System;

namespace BlogNET.Services
{
    public interface IBlogService
    {
        bool Import();

        bool Export();
    }

    public class BlogService
        : IBlogService
    {
        private readonly ILogger<BlogService> logger;

        public BlogService(
            ILogger<BlogService> logger)
        {
            this.logger = logger;
        }

        public bool Export()
        {
            throw new NotImplementedException();
        }

        public bool Import()
        {
            throw new NotImplementedException();
        }
    }
}