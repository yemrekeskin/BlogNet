﻿using BlogNET.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Services
{
    public interface ICommentService
         : IService<Comment>
    {

    }

    public class CommentService
        : BaseService<Comment>, ICommentService
    {
        public CommentService(ICommentRepository repo) 
            : base(repo)
        {

        }
    }
}
