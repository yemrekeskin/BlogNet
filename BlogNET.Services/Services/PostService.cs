﻿using BlogNET.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Services
{
    public interface IPostService
        : IService<Post>
    {

    }

    public class PostService
        : BaseService<Post>, IPostService
    {
        public PostService(IPostRepository repo) 
            : base(repo)
        {

        }
    }
}
