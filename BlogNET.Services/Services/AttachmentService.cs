﻿using BlogNET.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Services
{
    public interface IAttachmentService
        : IService<Attachment>
    {

    }

    public class AttachmentService
        : BaseService<Attachment>, IAttachmentService
    {
        public AttachmentService(IAttachmentRepository repo) 
            : base(repo)
        {

        }
    }
}
