﻿using BlogNET.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Services
{
    public interface IUserService
    {

    }

    public class UserService
        : IUserService
    {
        private readonly ILogger<UserService> logger;
        private readonly UserManager<User> userManager;

        public UserService(
            ILogger<UserService> logger,
            UserManager<User> userManager)
        {
            this.logger = logger;
            this.userManager = userManager;
        }
    }
}
