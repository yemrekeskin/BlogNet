﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Services
{
    public interface ICacheService
    {
        T Get<T>(string key);

        void Set<T>(string key, T value);

        T GetOrSet<T>(string key, T value);

        void Remove(string key);
    }

    public class CacheService
        : ICacheService
    {
        private readonly ILogger<CacheService> logger;
        private readonly IMemoryCache memoryCache;
        
        public CacheService(
            ILogger<CacheService> logger,
            IMemoryCache memoryCache)
        {
            this.logger = logger;
            this.memoryCache = memoryCache;
        }

        public T Get<T>(string key)
        {
            T cached;
            // cache de var ise getir
            if (memoryCache.TryGetValue<T>(key, out cached))
            {
                memoryCache.Get<T>(key);
            }
            return cached;
        }

        public void Remove(string key)
        {
            this.memoryCache.Remove(key);
        }

        public void Set<T>(string key, T value)
        {
            T cached;
            // cache de yok ise ekle
            if (!memoryCache.TryGetValue<T>(key, out cached))
            {
                var opt = new MemoryCacheEntryOptions();
                opt.SlidingExpiration = TimeSpan.FromHours(1);

                cached = value;
                memoryCache.Set(key, cached, opt);
            }
        }

        public T GetOrSet<T>(string key, T value)
        {
            T cached;
            // cache de yok ise ekle
            if (!memoryCache.TryGetValue<T>(key, out cached))
            {
                var opt = new MemoryCacheEntryOptions();
                opt.SlidingExpiration = TimeSpan.FromHours(1);

                cached = value;
                memoryCache.Set(key, cached, opt);
            }
            else
            {
                cached = memoryCache.Get<T>(key);
            }
            return cached;
        }
    }
}
