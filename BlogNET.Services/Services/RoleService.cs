﻿using BlogNET.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Services
{
    public interface IRoleService
    {

    }

    public class RoleService
        : IRoleService
    {
        private readonly ILogger<RoleService> logger;
        private readonly RoleManager<Role> roleManager;
        public RoleService(
            ILogger<RoleService> logger,
            RoleManager<Role> roleManager)
        {
            this.logger = logger;
            this.roleManager = roleManager;
        }
    }
}
