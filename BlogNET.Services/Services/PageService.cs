﻿using BlogNET.Models;
using BlogNET.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Services
{
    public interface IPageService
        : IService<Page>
    {

    }

    public class PageService
        : BaseService<Page>, IPageService
    {
        public PageService(IPageRepository repo) 
            : base(repo)
        {

        }
    }
}
