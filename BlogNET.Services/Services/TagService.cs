﻿using BlogNET.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Services
{
    public interface ITagService
        : IService<Tag>
    {

    }

    public class TagService
        : BaseService<Tag>, ITagService
    {
        public TagService(ITagRepository repo) 
            : base(repo)
        {

        }
    }
}
