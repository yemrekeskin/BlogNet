﻿using BlogNET.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlogNET.Services
{
    public interface ICategoryService
        : IService<Category>
    {  

    }

    public class CategoryService
        : BaseService<Category>, ICategoryService
    {
        public CategoryService(ICategoryRepository repo) 
            : base(repo)
        {

        }
    }
}
