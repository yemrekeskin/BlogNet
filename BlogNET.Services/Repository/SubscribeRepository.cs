﻿using BlogNET.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Services
{
    public interface ISubscribeRepository
        : IRepository<Subscribe>
    {

    }

    public class SubscribeRepository
        : BaseRepository<Subscribe>, ISubscribeRepository
    {
        public SubscribeRepository()
        {
            
        }

        public SubscribeRepository(IDbContext context)
            : base(context)
        {

        }
    }
}
