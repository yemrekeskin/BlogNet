﻿using BlogNET.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Services
{
    public interface ITagRepository
        : IRepository<Tag>
    {

    }

    public class TagRepository 
        : BaseRepository<Tag>, ITagRepository
    {
        public TagRepository()
        {

        }

        public TagRepository(IDbContext context)
            : base(context)
        {

        }
    }
}
