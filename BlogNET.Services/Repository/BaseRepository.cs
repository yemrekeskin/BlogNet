﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BlogNET.Models;
using Microsoft.EntityFrameworkCore;

namespace BlogNET.Services
{
    public interface IRepository<TEntity>
    {
        TEntity Get(long id, params Expression<Func<TEntity, object>>[] includeProperties);


        IQueryable<TEntity> List(params Expression<Func<TEntity, object>>[] includeProperties);
        IQueryable<TEntity> List(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties);
        IQueryable<TEntity> List(Expression<Func<TEntity, bool>> predicate);


        void Update(TEntity entity);
        void Add(TEntity entity);
        void AddBatch(ICollection<TEntity> entities);


        TEntity Remove(long id);
        void RemoveBatch(ICollection<TEntity> entities);
        void Remove(TEntity entity);


        int SaveChanges();
    }

    public abstract class BaseRepository<TEntity>
        : IRepository<TEntity>
        where TEntity : BaseEntity
    {
        private readonly IDbContext context;

        public BaseRepository(IDbContext context)
        {
            this.context = context;
        }

        public BaseRepository()
        {
            this.context = new ApplicationDbContext();
        }


        public virtual void Add(TEntity entity)
        {
            var table = context.Set<TEntity>();
            table.Add(entity);
            this.SaveChanges();
        }

        public virtual void AddBatch(ICollection<TEntity> entities)
        {
            var table = context.Set<TEntity>();
            foreach (var entity in entities)
            {
                table.Add(entity);
            }
            this.SaveChanges();
        }

        public virtual IQueryable<TEntity> List(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> entities = context.Set<TEntity>();
            if(includeProperties != null)
            {
                foreach (var includeProperty in includeProperties)
                {
                    entities = entities.Include(includeProperty);
                }
            }
            return entities;
        }

        public virtual IQueryable<TEntity> List(Expression<Func<TEntity, bool>> predicate)
        {
            var table = this.List();
            var result = table.Where(predicate);
            return result;
        }

        public virtual IQueryable<TEntity> List(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var table = this.List(includeProperties);
            var result = table.Where(predicate);
            return result;
        }

        public virtual TEntity Get(long id, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            var table = this.List(includeProperties);
            var result = table.SingleOrDefault(d => d.Id == id);
            return result;
        }

        public virtual TEntity Remove(long id)
        {
            TEntity entity = this.Get(id);
            entity.IsDeleted = true;
            this.Update(entity);

            return entity;
        }

        public virtual void Remove(TEntity entity)
        {
            entity.IsDeleted = true;
            this.Update(entity);
        }

        public virtual void RemoveBatch(ICollection<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                entity.IsDeleted = true;
            }
            this.context.SaveChanges();
        }
        
        public virtual void Update(TEntity entity)
        {
            this.SaveChanges();
        }
        

        public int SaveChanges()
        {
            return this.context.SaveChanges();
        }
    }
}