﻿using BlogNET.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Services
{
    public interface IAttachmentRepository
        : IRepository<Attachment>
    {

    }

    public class AttachmentRepository
        : BaseRepository<Attachment>, IAttachmentRepository
    {
        public AttachmentRepository()
        {
            
        }

        public AttachmentRepository(IDbContext context)
            : base(context)
        {

        }
    }
}
