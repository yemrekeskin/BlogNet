﻿using BlogNET.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Services
{
    public interface ICategoryRepository
        : IRepository<Category>
    {

    }

    public class CategoryRepository
        : BaseRepository<Category>, ICategoryRepository
    {
        public CategoryRepository()
        {

        }

        public CategoryRepository(IDbContext context)
            : base(context)
        {

        }
    }
}
