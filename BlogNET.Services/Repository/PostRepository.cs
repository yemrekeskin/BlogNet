﻿using BlogNET.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Services
{
    public interface IPostRepository
        : IRepository<Post>
    {

    }

    public class PostRepository
        : BaseRepository<Post>, IPostRepository
    {
        public PostRepository()
        {

        }

        public PostRepository(IDbContext context)
            : base(context)
        {

        }
    }
}
