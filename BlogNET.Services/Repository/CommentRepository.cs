﻿using BlogNET.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Services
{
    public interface ICommentRepository
         : IRepository<Comment>
    {

    }

    public class CommentRepository
        : BaseRepository<Comment>, ICommentRepository
    {
        public CommentRepository()
        {

        }

        public CommentRepository(IDbContext context)
            : base(context)
        {

        }
    }
}
