﻿using BlogNET.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Services
{
    public interface IPageRepository
         : IRepository<Page>
    {

    }

    public class PageRepository
        : BaseRepository<Page>, IPageRepository
    {
        public PageRepository()
        {

        }

        public PageRepository(IDbContext context)
            : base(context)
        {

        }
    }
}
