﻿using BlogNET.Models;
using Microsoft.EntityFrameworkCore;

namespace BlogNET.Services
{
    public class ApplicationDbContext
        : DbContext, IDbContext
    {
        public ApplicationDbContext()
        {

        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // BlogNET Entity Mapping
            var appSchema = "core";
            modelBuilder.Entity<PostCategory>(entity =>
            {
                entity.HasKey(d => new { d.CategoryId, d.PostId });
                entity.ToTable("PostCategory", appSchema);
            });
            modelBuilder.Entity<PostContributor>(entity => {
                entity.HasKey(d => new { d.PostId, d.UserId });
                entity.ToTable("PostContributor", appSchema);
            });
            modelBuilder.Entity<Attachment>(entity => {
                entity.ToTable("Attachments", appSchema);
            });
            modelBuilder.Entity<Category>(entity => {
                entity.ToTable("Categories", appSchema);
            });
            modelBuilder.Entity<Comment>(entity => {
                entity.ToTable("Comments", appSchema);
            });
            modelBuilder.Entity<Contributor>(entity => {
                entity.ToTable("Contributors", appSchema);
            });
            modelBuilder.Entity<Page>(entity => {
                entity.ToTable("Pages", appSchema);
            });
            modelBuilder.Entity<Post>(entity => {
                entity.ToTable("Posts", appSchema);
            });
            modelBuilder.Entity<Tag>(entity => {
                entity.ToTable("Tags", appSchema);
            });
            modelBuilder.Entity<Subscribe>(entity => {
                entity.ToTable("Subscribes", appSchema);
            });


            // Identity Entity Mapping
            var IdentitySchema = "auth";
            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("Users", IdentitySchema);
            });

            modelBuilder.Entity<UserLogin>(entity =>
            {
                entity.HasKey(d => new { d.LoginProvider, d.ProviderKey, d.UserId });
                entity.ToTable("UserLogins", IdentitySchema);
            });
            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasKey(d => new { d.UserId, d.RoleId });
                entity.ToTable("UserRoles", IdentitySchema);
            });

            modelBuilder.Entity<UserClaim>(entity =>
            {
                entity.ToTable("UserClaims", IdentitySchema);
            });

            modelBuilder.Entity<UserToken>(entity =>
            {
                entity.HasKey(d => new { d.LoginProvider, d.UserId });
                entity.ToTable("UserTokens", IdentitySchema);
            });
            
            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("Roles", IdentitySchema);
            });

            modelBuilder.Entity<RoleClaim>(entity =>
            {
                entity.ToTable("RoleClaims", IdentitySchema);
            });

            base.OnModelCreating(modelBuilder);
        }


        public override int SaveChanges()
        {
            return base.SaveChanges();
        }

        // BlogNET En

        public DbSet<Category> Categories { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Subscribe> Subscribes { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<Contributor> Contributors { get; set; }

        // Identity Entitites

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RoleClaim> RoleClaims { get; set; }
        public DbSet<UserClaim> UserClaimns { get; set; }
        public DbSet<UserLogin> UserLogins { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<UserToken> UserTokens { get; set; }

    }
}