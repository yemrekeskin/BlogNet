﻿using BlogNET.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlogNET.Services
{
    public static class ContextExtention
    {
        public static void EnsureSeeded(this ApplicationDbContext context)
        {
            if (!context.Categories.Any())
            {
                var category = new Category()
                {
                    CategoryName = "SampleCategory",
                    CreatedBy = "System",
                    UpdatedBy = "System",
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    IsDeleted = false,
                    ActivePassive = ActivePassive.Active,
                };

                context.Categories.Add(category);
                context.SaveChanges();
            }

            if (!context.Tags.Any())
            {
                var tag = new Tag()
                {
                    TagName = "SampleTag",
                    CreatedBy = "System",
                    UpdatedBy = "System",
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    IsDeleted = false,
                    ActivePassive = ActivePassive.Active,
                };

                context.Tags.Add(tag);
                context.SaveChanges();
            }

            if (!context.Comments.Any())
            {
                var comment = new Comment()
                {
                    Name = "Sample",
                    CommentState = CommentState.Pending,
                    Email = "SampleEmail",
                    IsRoot = true,
                    CreatedBy = "System",
                    UpdatedBy = "System",
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    IsDeleted = false,
                    ActivePassive = ActivePassive.Active,
                };

                context.Comments.Add(comment);
                context.SaveChanges();
            }

            if (!context.Pages.Any())
            {
                var page = new Page()
                {
                    CreatedBy = "System",
                    UpdatedBy = "System",
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    IsDeleted = false,
                    ActivePassive = ActivePassive.Active,
                };

                context.Pages.Add(page);
                context.SaveChanges();
            }

            if (!context.Posts.Any())
            {
                var post = new Post()
                {
                    CreatedBy = "System",
                    UpdatedBy = "System",
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    IsDeleted = false,
                    ActivePassive = ActivePassive.Active,
                };

                context.Posts.Add(post);
                context.SaveChanges();
            }

            if (!context.Attachments.Any())
            {
                var attachment = new Attachment()
                {
                    CreatedBy = "System",
                    UpdatedBy = "System",
                    CreatedDate = DateTime.Now,
                    UpdatedDate = DateTime.Now,
                    IsDeleted = false,
                    ActivePassive = ActivePassive.Active,
                };

                context.Attachments.Add(attachment);
                context.SaveChanges();
            }


        }
    }
}
