﻿using BlogNET.Models;
using Microsoft.Extensions.Logging;
using System;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace BlogNET.Services
{
    public interface ISmsSender
    {
        bool Send(SmsMessage smsMessage);
    }

    /// <summary>
    /// https://www.twilio.com/blog/2017/02/next-generation-csharp-helper-library-release.html
    /// </summary>
    public class SmsSender
        : ISmsSender
    {
        private readonly ILogger<SmsSender> logger;

        public SmsSender(
            ILogger<SmsSender> logger)
        {
            this.logger = logger;
        }

        public bool Send(SmsMessage smsMessage)
        {
            bool Succeed = true;
            var config = new SmsConfig();

            var to = new PhoneNumber(smsMessage.To);
            var from = new PhoneNumber(config.From);

            try
            {
                TwilioClient.Init(config.AccountSID, config.AuthToken);

                MessageResource.Create(
                        to,
                        from: from,
                        body: $"Hello Guys !! Welcome to Asp.Net Core With Twilio SMS API");
            }
            catch (Exception ex)
            {
                logger.LogError(ex, ex.Message);
                Succeed = false;
            }
            return Succeed;
        }
    }
}