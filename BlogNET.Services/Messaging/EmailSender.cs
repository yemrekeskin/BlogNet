﻿using BlogNET.Models;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Logging;
using MimeKit;
using MimeKit.Text;
using System;
using System.Linq;

namespace BlogNET.Services
{
    public interface IEmailSender
    {
        bool Send(EmailMessage emailMessage);
    }

    public class EmailSender
        : IEmailSender
    {
        private readonly ILogger<EmailSender> logger;

        public EmailSender(
            ILogger<EmailSender> logger)
        {
            this.logger = logger;
        }

        public bool Send(EmailMessage emailMessage)
        {
            var Succeed = true;

            var config = new EmailConfig();

            var message = new MimeMessage();
            message.Subject = emailMessage.Subject;
            message.Body = new TextPart(TextFormat.Html)
            {
                Text = emailMessage.Content
            };
            message.From.Add(new MailboxAddress(config.Name, config.SmtpUsername));
            message.To.AddRange(emailMessage.To.Select(d => new MailboxAddress(emailMessage.Subject, d)));

            using (var emailClient = new SmtpClient())
            {
                try
                {
                    emailClient.Connect(config.SmtpServer, config.SmtpPort);
                    emailClient.Authenticate(config.SmtpUsername, config.SmtpPassword);
                    emailClient.Send(message);
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, ex.Message);
                    Succeed = false;
                }
                finally
                {
                    emailClient.Disconnect(true);
                }
            }
            return Succeed;
        }
    }
}