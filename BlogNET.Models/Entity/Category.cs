﻿using System;
using System.Collections.Generic;

namespace BlogNET.Models
{
    public class Category
        : BaseEntity
    {
        public string CategoryName { get; set; }

        public ICollection<PostCategory> PostCategory { get; set; }
    }
}
