﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Models
{
    public class Subscribe
        : BaseEntity
    {
        public string Email { get; set; }

        public SubscribeState State { get; set; }
    }
}
