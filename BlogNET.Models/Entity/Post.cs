﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Models
{
    public class Post
        : BaseEntity
    {
        public string Caption { get; set; }
        public string Content { get; set; }

        public PostLevel PostLevel { get; set; }
        public PublishState PublishState { get; set; }
        public VisibilityState VisibilityState { get; set; }
        
        public int ViewCount { get; set; }

        public ICollection<PostCategory> PostCategory { get; set; }
        public ICollection<Tag> Tags { get; set; }
        public ICollection<Attachment> Attachments { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<PostContributor> PostContributors { get; set; }

        public long UserId { get; set; }
    }
}
