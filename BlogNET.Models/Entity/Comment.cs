﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BlogNET.Models
{
    public class Comment
        : BaseEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public CommentState CommentState { get; set; }


        public bool IsRoot { get; set; }
        public long ParentId { get; set; }
        
        public ICollection<Comment> Replies { get; set; }

        public Post Post { get; set; }
        public int PostId { get; set; }

        public Page Page { get; set; }
        public int PageId { get; set; }        
    }
}
