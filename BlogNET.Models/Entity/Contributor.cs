﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Models
{
    public class Contributor
        : BaseEntity
    {
        public string Name { get; set; }
        
        public ICollection<PostContributor> PostContributors { get; set; }
    }
}
