﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Models
{
    public class Tag
        : BaseEntity
    {
        public string TagName { get; set; }

        public Page Page { get; set; }
        public int PageId { get; set; }


        public Post Post { get; set; }
        public int PostId { get; set; }

    }
}
