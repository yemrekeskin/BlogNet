﻿namespace BlogNET.Models
{
    public class Attachment
        : BaseEntity
    {
        public byte[] File { get; set; }
        
        public Page Page { get; set; }
        public int PageId { get; set; }


        public Post Post { get; set; }
        public int PostId { get; set; }

    }
}