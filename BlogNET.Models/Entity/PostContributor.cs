﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Models
{
    public class PostContributor
    {
        public Post Post { get; set; }
        public int PostId { get; set; }

        public User User { get; set; }
        public long UserId { get; set; }
    }
}
