﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Models
{
    public abstract class BaseEntity
        : IEntity 
    {        
        public string UniqueKey { get; set; }

        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public bool IsDeleted { get; set; }

        public ActivePassive ActivePassive { get; set; }
        public int Id { get; set; }

        public BaseEntity()
        {
            this.UniqueKey = Guid.NewGuid().ToString();
        }
    }
}
