﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Models
{
    public enum VisibilityState
    {
        Public = 0,
        Private = 1,
        Protected = 2
    }
}
