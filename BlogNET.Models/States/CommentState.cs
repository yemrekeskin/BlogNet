﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Models
{
    public enum CommentState
    {
        Pending = 0,
        Approved = 1,
        Spam = 2,
        Trash = 3
    }
}
