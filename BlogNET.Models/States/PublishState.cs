﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Models
{
    public enum PublishState
    {
        Draft = 0,
        PendingReviewed = 1,
        Published = 2,
        Deleted = 3
    }
}
