﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Models
{
    public enum PostLevel
    {
        NotSpecified = 0,
        Beginner = 1,
        Medium = 2,
        Advanced = 3
    }
}
