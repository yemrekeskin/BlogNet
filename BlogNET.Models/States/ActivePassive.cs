﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Models
{
    public enum ActivePassive
    {
        Active = 0,
        Passive = 1
    }
}
