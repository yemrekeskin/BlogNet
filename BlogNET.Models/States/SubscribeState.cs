﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Models
{
    public enum SubscribeState
    {
        Subscribe = 0,
        UnSubscribe = 1,
        Suspended = 2
    }
}
