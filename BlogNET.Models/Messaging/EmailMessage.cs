﻿using System.Collections.Generic;

namespace BlogNET.Models
{
    public class EmailMessage
    {
        public List<string> To { get; set; }

        public string Subject { get; set; }
        public string Content { get; set; }
    }
}