﻿using BlogNET.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Tests
{
    public class TestDatas
    {
        public Category Category => new Category
        {
            CategoryName = "SampleCategory",
            CreatedBy = "System",
            UpdatedBy = "System",
            CreatedDate = DateTime.Now,
            UpdatedDate = DateTime.Now,
            IsDeleted = false,
            ActivePassive = ActivePassive.Active,
        };

        public Comment Comment => new Comment
        {
            Email = "yemrekeskin@gmail.com",
            CommentState = CommentState.Approved,
            CreatedBy = "System",
            UpdatedBy = "System",
            CreatedDate = DateTime.Now,
            UpdatedDate = DateTime.Now,
            IsDeleted = false,
            ActivePassive = ActivePassive.Active
        };

        public Tag Tag => new Tag
        {
            CreatedBy = "System",
            UpdatedBy = "System",
            CreatedDate = DateTime.Now,
            UpdatedDate = DateTime.Now,
            IsDeleted = false,
            ActivePassive = ActivePassive.Active
        };

        public Page Page => new Page
        {
            CreatedBy = "System",
            UpdatedBy = "System",
            CreatedDate = DateTime.Now,
            UpdatedDate = DateTime.Now,
            IsDeleted = false,
            ActivePassive = ActivePassive.Active
        };

        public Post Post => new Post
        {
            CreatedBy = "System",
            UpdatedBy = "System",
            CreatedDate = DateTime.Now,
            UpdatedDate = DateTime.Now,
            IsDeleted = false,
            ActivePassive = ActivePassive.Active
        };

        public Subscribe Subscribe => new Subscribe
        {
            CreatedBy = "System",
            UpdatedBy = "System",
            CreatedDate = DateTime.Now,
            UpdatedDate = DateTime.Now,
            IsDeleted = false,
            ActivePassive = ActivePassive.Active
        };

        public Attachment Attachment => new Attachment
        {
            CreatedBy = "System",
            UpdatedBy = "System",
            CreatedDate = DateTime.Now,
            UpdatedDate = DateTime.Now,
            IsDeleted = false,
            ActivePassive = ActivePassive.Active
        };
    }
}
