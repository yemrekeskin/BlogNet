﻿using BlogNET.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace BlogNET.Tests
{
    public class TestContext
        : IDisposable
    {
        public TestDatas TestDatas { get; set; }
        
        public IDbContext DbContext { get; }


        public ICategoryRepository CategoryRepository { get; set; }
        public ITagRepository TagRepository { get; set; }
        public ICommentRepository CommentRepository { get; set; }
        public IPageRepository PageRepository { get; set; }
        public IPostRepository PostRepository { get; set; }
        public ISubscribeRepository SubscribeRepository { get; set; }

        public ICategoryService CategoryService { get; set; }
        public ITagService TagService { get; set; }
        public ICommentService CommentService { get; set; }
        public IPageService PageService { get; set; }
        public IPostService PostService { get; set; }
        public ISubscribeService SubscribeService { get; set; }
        
        public TestContext()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
               .UseInMemoryDatabase(databaseName: "ApplicationInMemoryDbContext")
               .Options;

            this.DbContext = new ApplicationDbContext(options);

            this.CategoryRepository = new CategoryRepository(DbContext);
            this.CommentRepository = new CommentRepository(DbContext);
            this.TagRepository = new TagRepository(DbContext);
            this.PageRepository = new PageRepository(DbContext);
            this.PostRepository = new PostRepository(DbContext);
            this.SubscribeRepository = new SubscribeRepository(DbContext);

            this.CategoryService = new CategoryService(this.CategoryRepository);
            this.CommentService = new CommentService(this.CommentRepository);
            this.TagService = new TagService(this.TagRepository);
            this.PageService = new PageService(this.PageRepository);
            this.PostService = new PostService(this.PostRepository);
            this.SubscribeService = new SubscribeService(this.SubscribeRepository);

            this.TestDatas = new TestDatas();
        }

        public void Dispose()
        {
        }
    }
}
