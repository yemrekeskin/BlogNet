using BlogNET.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace BlogNET.Tests
{
    [TestClass]
    public class CategoryServiceTests
    {
        private readonly TestContext context = null;
        private readonly TestDatas data = null;
        private readonly ICategoryService service = null;

        public CategoryServiceTests()
        {
            this.context = new TestContext();
            this.data = this.context.TestDatas;
            this.service = this.context.CategoryService;
        }

        [TestMethod]
        public void Add()
        {
            var category = data.Category;
            category.UniqueKey = Guid.NewGuid().ToString();

            service.Add(category);
            var result = service.List();
        }
    }
}
