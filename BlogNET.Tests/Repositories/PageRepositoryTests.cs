﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BlogNET.Services;
using BlogNET.Models;
using Microsoft.EntityFrameworkCore;

namespace BlogNET.Tests
{
    [TestClass]
    public class PageRepositoryTests
    {
        [TestMethod]
        public void Add()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: "ApplicationInMemoryDbContext")
                .Options;

            // Run the test against one instance of the context
            using (var context = new ApplicationDbContext(options))
            {
                var repo = new CategoryRepository(context);

                Category model = new Category();
                model.CategoryName = "new category";
                repo.Add(model);

                var list = repo.List();
            }
            
        }
    }
}