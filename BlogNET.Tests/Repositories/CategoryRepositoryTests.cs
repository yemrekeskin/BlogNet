﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BlogNET.Services;
using BlogNET.Models;
using Microsoft.EntityFrameworkCore;

namespace BlogNET.Tests
{
    [TestClass]
    public class CategoryRepositoryTests
    {
        private readonly TestContext context = null;
        private readonly TestDatas data = null;
        private ICategoryRepository repo = null;

        public CategoryRepositoryTests()
        {
            this.context = new TestContext();
            this.repo = this.context.CategoryRepository;
            this.data = this.context.TestDatas;
        }
        

        [TestMethod]
        public void Add()
        {
            var category = data.Category;
            this.repo.Add(category);
            var list = repo.List();
        }
    }
}