﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace BlogNET.Migrations
{
    public partial class ChangedTableNameAndSchema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "core");

            migrationBuilder.EnsureSchema(
                name: "auth");

            migrationBuilder.RenameTable(
                name: "Users",
                newSchema: "auth");

            migrationBuilder.RenameTable(
                name: "UserRoles",
                newSchema: "auth");

            migrationBuilder.RenameTable(
                name: "UserLogins",
                newSchema: "auth");

            migrationBuilder.RenameTable(
                name: "Tags",
                newSchema: "core");

            migrationBuilder.RenameTable(
                name: "Subscribes",
                newSchema: "core");

            migrationBuilder.RenameTable(
                name: "Roles",
                newSchema: "auth");

            migrationBuilder.RenameTable(
                name: "RoleClaims",
                newSchema: "auth");

            migrationBuilder.RenameTable(
                name: "Posts",
                newSchema: "core");

            migrationBuilder.RenameTable(
                name: "PostContributor",
                newSchema: "core");

            migrationBuilder.RenameTable(
                name: "PostCategory",
                newSchema: "core");

            migrationBuilder.RenameTable(
                name: "Pages",
                newSchema: "core");

            migrationBuilder.RenameTable(
                name: "Contributors",
                newSchema: "core");

            migrationBuilder.RenameTable(
                name: "Comments",
                newSchema: "core");

            migrationBuilder.RenameTable(
                name: "Categories",
                newSchema: "core");

            migrationBuilder.RenameTable(
                name: "Attachments",
                newSchema: "core");

            migrationBuilder.CreateTable(
                name: "UserTokens",
                schema: "auth",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    UserId = table.Column<long>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTokens", x => new { x.LoginProvider, x.UserId });
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserTokens",
                schema: "auth");

            migrationBuilder.RenameTable(
                name: "Tags",
                schema: "core");

            migrationBuilder.RenameTable(
                name: "Subscribes",
                schema: "core");

            migrationBuilder.RenameTable(
                name: "Posts",
                schema: "core");

            migrationBuilder.RenameTable(
                name: "PostContributor",
                schema: "core");

            migrationBuilder.RenameTable(
                name: "PostCategory",
                schema: "core");

            migrationBuilder.RenameTable(
                name: "Pages",
                schema: "core");

            migrationBuilder.RenameTable(
                name: "Contributors",
                schema: "core");

            migrationBuilder.RenameTable(
                name: "Comments",
                schema: "core");

            migrationBuilder.RenameTable(
                name: "Categories",
                schema: "core");

            migrationBuilder.RenameTable(
                name: "Attachments",
                schema: "core");

            migrationBuilder.RenameTable(
                name: "Users",
                schema: "auth");

            migrationBuilder.RenameTable(
                name: "UserRoles",
                schema: "auth");

            migrationBuilder.RenameTable(
                name: "UserLogins",
                schema: "auth");

            migrationBuilder.RenameTable(
                name: "Roles",
                schema: "auth");

            migrationBuilder.RenameTable(
                name: "RoleClaims",
                schema: "auth");
        }
    }
}
