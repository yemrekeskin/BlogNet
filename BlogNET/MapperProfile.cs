﻿using AutoMapper;
using BlogNET.Models;
using BlogNET.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogNET
{
    public class MapperProfile
        : Profile
    {
        public MapperProfile()
        {
            CreateMap<CategoryViewModel, Category>();
            CreateMap<Category, CategoryViewModel>();

            CreateMap<TagViewModel, Tag>();
            CreateMap<Tag, TagViewModel>();

            CreateMap<PageViewModel, Page>();
            CreateMap<Page, PageViewModel>();

            CreateMap<PostViewModel, Post>();
            CreateMap<Post, PostViewModel>();

            CreateMap<CommentViewModel, Comment>();
            CreateMap<Comment, CommentViewModel>();

            CreateMap<AttachmentViewModel, Attachment>();
            CreateMap<Attachment, AttachmentViewModel>();

            CreateMap<SubscribeViewModel, Subscribe>();
            CreateMap<Subscribe, SubscribeViewModel>();
        }
    }
}
