﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogNET.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BlogNET.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class SubscribeController 
        : Controller
    {
        private readonly ILogger<SubscribeController> logger;
        private readonly ISubscribeService service;

        public SubscribeController(
            ILogger<SubscribeController> logger,
            ISubscribeService service)
        {
            this.logger = logger;
            this.service = service;
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}