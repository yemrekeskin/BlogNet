﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogNET.Models;
using BlogNET.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BlogNET.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class RoleController 
        : Controller
    {
        private readonly ILogger<RoleController> logger;
        private readonly IRoleService roleService;

        public RoleController(
            ILogger<RoleController> logger,
            IRoleService roleService)
        {
            this.roleService = roleService;
            this.logger = logger;
        }

        public IActionResult Index()
        {
            var role = new Role();
            role.Name = "AdminRole";

            //Task<IdentityResult> result = roleManager.CreateAsync(role);
            

            return View();
        }
    }
}