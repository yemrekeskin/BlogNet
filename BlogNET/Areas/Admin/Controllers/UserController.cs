﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogNET.Models;
using BlogNET.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BlogNET.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class UserController 
        : Controller
    {
        private readonly ILogger<UserController> logger;
        private readonly IUserService userService;
        private readonly IMessagingService messagingService;

        public UserController(
            ILogger<UserController> logger,
            IUserService userService,
            IMessagingService messagingService)
        {
            this.logger = logger;
            this.userService = userService;
            this.messagingService = messagingService;
        }

        public IActionResult Index()
        {
            var user = new User();
            user.UserName = "admin";
            user.Password = "sazdgfsdfgsd";

            //userService.CreateAsync(user);

            var msg = new EmailMessage();
            msg.To.Add("yemrekeskin@outlook.com");
            msg.Subject = "SampleThat";
            msg.Content = "Account Created";

            messagingService.SendMail(msg);

            var smsMessage = new SmsMessage();
            smsMessage.To = "+90 543 506 3818";
            smsMessage.Message = "Hey You!!!";
            messagingService.SendSms(smsMessage);

            return View();
        }
    }
}