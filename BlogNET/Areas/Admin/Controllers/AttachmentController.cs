﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogNET.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BlogNET.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class AttachmentController 
        : Controller
    {
        private readonly IAttachmentService service;
        private readonly ILogger<AttachmentController> logger;

        public AttachmentController(
            ILogger<AttachmentController> logger,
            IAttachmentService service)
        {
            this.logger = logger;
            this.service = service;
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}