﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogNET.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BlogNET.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class PostController 
        : Controller
    {
        private readonly ILogger<PostController> logger;
        private readonly IPostService service;

        public PostController(
            ILogger<PostController> logger,
            IPostService service)
        {
            this.logger = logger;
            this.service = service;
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}