﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BlogNET.Models;
using BlogNET.Services;
using BlogNET.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BlogNET.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class CategoryController 
        : Controller, IController<CategoryViewModel>
    {
        private readonly ILogger<CategoryController> logger;
        private readonly ICategoryService service;

        public CategoryController(
            ILogger<CategoryController> logger,
            ICategoryService service)
        {
            this.logger = logger;
            this.service = service;
        }

        public IActionResult Index()
        {
            var result = service.List();
            var list = result.Select(d => new CategoryViewModel()
            {
                Id = d.Id,
                CategoryName = d.CategoryName,
                ActivePassive = d.ActivePassive
            });

            var viewModel = new CategoryListViewModel();
            viewModel.CategoryViewModels = list.ToList();

            return View(viewModel);
        }

        public IActionResult Create()
        {
            return View();
        }

        public IActionResult Create(CategoryViewModel viewModel)
        {
            try
            {
                var model = Mapper.Map<Category>(viewModel);
                service.Add(model);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                logger.LogError(ex, ex.Message);
            }

            return View();
        }

        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            return View();
        }

        public IActionResult Delete(int? id, CategoryViewModel viewModel)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var model = Mapper.Map<Category>(viewModel);
                service.Remove(model);

                return RedirectToAction(nameof(Index));
            }
            catch(Exception ex)
            {
                logger.LogError(ex, ex.Message);
            }

            return View();
        }

        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Id = (long)id;
            var viewModel = service.Get(Id);

            return View(viewModel);
        }

        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var Id = (long)id;
            var viewModel = service.Get(Id);

            return View(viewModel);
        }

        public IActionResult Edit(int? id, CategoryViewModel viewModel)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                    var model = Mapper.Map<Category>(viewModel);
                    service.Update(model);

                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return View("Edit", viewModel);
                } 
            }
            catch (Exception ex)
            {
                logger.LogError(ex, ex.Message);
            }

            return View();
        }
    }
}