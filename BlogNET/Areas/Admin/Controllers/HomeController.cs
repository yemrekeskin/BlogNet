﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BlogNET.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class HomeController 
        : Controller
    {
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly ILogger<HomeController> logger;

        public HomeController(
            IHttpContextAccessor httpContextAccessor,
            ILogger<HomeController> logger)
        {
            this.httpContextAccessor = httpContextAccessor;
            this.logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Test()
        {
            return View();
        }

    }
}