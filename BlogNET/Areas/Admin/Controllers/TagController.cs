﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogNET.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BlogNET.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class TagController 
        : Controller
    {
        private readonly ILogger<TagController> logger;
        private readonly ITagService service;
        
        public TagController(
            ILogger<TagController> logger,
            ITagService service)
        {
            this.logger = logger;
            this.service = service;
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}