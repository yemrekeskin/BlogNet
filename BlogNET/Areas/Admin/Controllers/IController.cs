﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BlogNET.Areas.Admin.Controllers
{
    public interface IController<TViewModel>
    {
        IActionResult Index();

        IActionResult Details(int? id);
        

        IActionResult Create();

        [HttpPost]
        [ValidateAntiForgeryToken]
        IActionResult Create(TViewModel viewmodel);

        IActionResult Edit(int? id);
        [HttpPost]
        [ValidateAntiForgeryToken]
        IActionResult Edit(int? id, TViewModel viewModel);


        IActionResult Delete(int? id);

        [HttpPost]
        [ValidateAntiForgeryToken]
        IActionResult Delete(int? id, TViewModel viewModel);
    }
}