﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogNET.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BlogNET.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class CommentController 
        : Controller
    {
        private readonly ILogger<CommentController> logger;
        private readonly ICommentService service;
        
        public CommentController(
            ILogger<CommentController> logger,
            ICommentService service)
        {
            this.logger = logger;
            this.service = service;
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}