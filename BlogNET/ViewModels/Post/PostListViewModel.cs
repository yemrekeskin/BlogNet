﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogNET.ViewModels
{
    public class PostListViewModel
        : BaseViewModel
    {
        public List<PostViewModel> PostViewModels { get; set; }
    }
}
