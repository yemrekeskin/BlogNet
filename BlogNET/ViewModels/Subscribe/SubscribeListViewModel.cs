﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogNET.ViewModels
{
    public class SubscribeListViewModel
        : BaseViewModel
    {
        public List<SubscribeViewModel> SubscribeViewModels { get; set; }
    }
}
