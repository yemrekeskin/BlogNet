﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogNET.ViewModels
{
    public class TagListViewModel
        : BaseViewModel
    {
        public List<TagViewModel> TagViewModels { get; set; }
    }
}
