﻿using BlogNET.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogNET.ViewModels
{
    public interface IViewModel
    {

    }

    public abstract class BaseViewModel
        : IViewModel
    {
        public long Id { get; set; }
        public ActivePassive ActivePassive { get; set; }
        public bool IsDeleted { get; set; }
    }
}
