﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogNET.ViewModels
{
    public class CommentListViewModel
        : BaseViewModel
    {
        public List<CommentViewModel> CommentViewModels { get; set; }
    }
}
