﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogNET.ViewModels
{
    public class UserListViewModel
        : BaseViewModel
    {
        public List<UserViewModel> UserViewModels { get; set; }

    }
}
