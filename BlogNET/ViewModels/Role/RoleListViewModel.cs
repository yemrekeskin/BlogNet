﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogNET.ViewModels
{
    public class RoleListViewModel
        : BaseViewModel
    {
        public List<RoleViewModel> RoleViewModels { get; set; }

    }
}
