﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogNET.ViewModels
{
    public class PageListViewModel
        : BaseViewModel
    {
        public List<PageViewModel> PageViewModels { get; set; }
    }
}
