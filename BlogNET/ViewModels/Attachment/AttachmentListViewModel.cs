﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogNET.ViewModels
{
    public class AttachmentListViewModel
        : BaseViewModel
    {
        public List<AttachmentViewModel> AttachmentViewModels { get; set; }
    }
}
