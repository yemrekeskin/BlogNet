﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BlogNET.Controllers
{
    public class HomeController
        : BaseController
    {
        private readonly ILogger<HomeController> logger;

        public HomeController(ILogger<HomeController> logger)
        {
            this.logger = logger;
        }


        public IActionResult Index()
        {
            logger.LogInformation("Index page says hello");

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }
    }
}