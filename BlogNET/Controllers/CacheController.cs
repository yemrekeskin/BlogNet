﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogNET.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace BlogNET.Controllers
{
    public class CacheController 
        : BaseController
    {
        private readonly ILogger<CacheController> logger;
        private readonly ICacheService cacheService;

        public CacheController(
            ILogger<CacheController> logger,
            ICacheService cacheService)
        {
            this.logger = logger;
            this.cacheService = cacheService;
        }
        
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public string Get()
        {
            ViewBag.Result = String.Empty;

            var cacheKey = "TheTime";
            DateTime current = DateTime.Now;

            ViewBag.Result = cacheService.GetOrSet(cacheKey, current);
            

            return ViewBag.Result.ToString();
        }
    }
}