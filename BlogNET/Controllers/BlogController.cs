﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Text;

namespace BlogNET.Controllers
{
    public class BlogController
        : BaseController
    {
        private readonly ILogger<BlogController> logger;

        public BlogController(ILogger<BlogController> logger)
        {
            this.logger = logger;
        }

        //[Route("/robots.txt")]
        //public string RobotsTxt()
        //{
        //    string host = Request.Scheme + "://" + Request.Host;

        //    var sb = new StringBuilder();
        //    sb.AppendLine("User-agent: *");
        //    sb.AppendLine("Disallow:");
        //    sb.AppendLine($"sitemap: {host}/sitemap.xml");

        //    return sb.ToString();
        //}

        //[Route("/sitemap.xml")]
        //public void SitemapXml()
        //{
        //    Response.ContentType = "application/xml";
        //}

        //[Route("/Rss")]
        //public void Rss()
        //{
        //    Response.ContentType = "application/xml";
        //}

        //[Route("/Atom")]
        //public void Atom()
        //{
        //    Response.ContentType = "application/xml";
        //}

        //[Route("/humans.txt")]
        //public void Humans()
        //{
        //    Response.ContentType = "application/xml";
        //}

        //[Route("/opensearch.xml")]
        //public void Opensearch()
        //{
        //    Response.ContentType = "application/xml";
        //}
    }
}