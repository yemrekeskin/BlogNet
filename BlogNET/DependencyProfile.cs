﻿using BlogNET.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogNET
{
    public static class DependencyProfile
    {
        public static void DependencyLoad(this IServiceCollection services)
        {
            // messaging
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<ISmsSender, SmsSender>();
            services.AddTransient<IMessagingService, MessagingService>();

            // cache
            services.AddTransient<ICacheService, CacheService>();

            // context
            services.AddSingleton<IDbContext, ApplicationDbContext>();

            // repository and services for entities
            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<ICategoryService, CategoryService>();

            services.AddSingleton<ICommentRepository, CommentRepository>();
            services.AddSingleton<ICommentService, CommentService>();

            services.AddSingleton<ITagRepository, TagRepository>();
            services.AddSingleton<ITagService, TagService>();

            services.AddSingleton<IPageRepository, PageRepository>();
            services.AddSingleton<IPageService, PageService>();

            services.AddSingleton<IPostRepository, PostRepository>();
            services.AddSingleton<IPostService, PostService>();

            services.AddSingleton<ISubscribeRepository, SubscribeRepository>();
            services.AddSingleton<ISubscribeService, SubscribeService>();

            services.AddSingleton<IAttachmentRepository, AttachmentRepository>();
            services.AddSingleton<IAttachmentService, AttachmentService>();
            

        }
    }
}
